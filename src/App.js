import {useState} from 'react';

function App() {
  const [calcul, setCalcul] = useState("");

  
  const updateCalcul = value =>{
    setCalcul(calcul + value);
  }

  //afin de ne pas répéter <button>1,2,3,4...</button>
  const createNumbers = () => {
    const digits = [];
    for (let i = 0; i <10; i++ ){
      digits.push(
        <button  onClick={() => updateCalcul(i.toString())}key = {i} > {i}</button>
      )
    }
    return digits;
  }

  //Afficher le résultats quand on appuie sur =
  const calculer = () => {
    setCalcul(eval(calcul).toString());
  }

  const deleteResultat = () => {
    if (calcul === '' ){
      return; 
    }
    const value = calcul.slice(0, -1);
    setCalcul(value);
  }

  return (
    <div className="App">
      <div className="calculator">
        <div className="display">
          {calcul || "0"}
        </div>
        <div className="operators">
          <button onClick={() => updateCalcul('+')}>+</button>
          <button onClick={() => updateCalcul('-')}>-</button>
          <button onClick={() => updateCalcul('*')}>*</button>
          <button onClick={() => updateCalcul('/')}>/</button>
          <button onClick={deleteResultat}>Clear</button>
        </div>
        <div className="digits">
          {createNumbers()}
          <button onClick={() => updateCalcul('.')}>.</button>
          <button onClick={calculer}>=</button>
        </div>
      </div>
    </div>
  );
}

export default App;
